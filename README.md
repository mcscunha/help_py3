# Help Py3

Pequenos projetos de Python3 que ajudam a entender como usar os "import".
Alguns códigos ou comandos que normalmente geram dúvidas.
Tem várias versões do mesmo código para exemplificar outros casos.

Neste caso, falo do uso de 
- import
- exec
- carregamento de modulos

Quais as diferencas em usar:

## Casos

1. Maior Limitação

```python
import tkinter
...
...
def tela2():
    exec(open(arq_py).read())
...
...
```

2. Menor Limitação - Sem import local

```python
import tkinter
...
...
def tela2():
    exec(open(arq_py).read(), {})
...
...
```

3. Levando variaveis local E usando import local

```python
import tkinter
...
...
def tela2():
    import banco
    import tkinter
    from tkinter import ttk
    exec(open(arq_py).read(), locals())
...
...
```

4. Levando variaveis globais

```python
import tkinter
...
...
def tela2():
    exec(open(arq_py).read(), globals())
...
...
```
