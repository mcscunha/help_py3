# Grava no Banco de Dados notas.db

#
# como foi passado um contexto local, o sistema deixa importar bibliotecas
# no topo do codigo
#
import tkinter
from tkinter import ttk, messagebox
import banco


def popular():
    global tv
    p_treeview = tv
    p_treeview.delete(*p_treeview.get_children())
    vquery="SELECT * FROM tb_contato order by ID"
    linhas = banco.dql(vquery)
    for i in linhas:
        p_treeview.insert("", "end", values=i)


# Rotina para Deletar Dados Formulario:
def deletar():
    vid=-1
    itemSelecionado=tv.selection()[0]
    valores=tv.item(itemSelecionado,"values")
    vid=valores[0]
    try:
        vquery="DELETE FROM tb_contato WHERE id="+vid
        banco.dml(vquery)
    except:
        tkinter.messagebox.showinfo(title="ERRO", message="Erro ao deletar")
        return
    tv.delete(itemSelecionado)

# Rotina para Pesquisar Dados Formulario:
def pesquisar():
    global vnomepesquisar
    tv.delete(*tv.get_children())
    vquery="SELECT * FROM tb_contato WHERE nome LIKE '%"+vnomepesquisar.get()+"%' order by ID"
    linhas=banco.dql(vquery)
    for i in linhas:
        tv.insert("", "end", values=i)



app = tkinter.Tk()
app.title("Cadastro Notas")
app.geometry("600x450")

##########Tabema Pesquisar, Todos e Deletar###########
quadroPesquisar=tkinter.LabelFrame(app,text="Pesquisar contatos")
quadroPesquisar.pack(fill="both",expand="yes",padx=10,pady=10)

Lbid=tkinter.Label(quadroPesquisar,text="Nome")
Lbid.pack(side="left")
vnomepesquisar=tkinter.Entry(quadroPesquisar)
vnomepesquisar.pack(side="left",padx=10)
btn_pesquisar=tkinter.Button(quadroPesquisar,text="Pesquisar",command=pesquisar)
btn_pesquisar.pack(side="left",padx=10)
btn_todos=tkinter.Button(quadroPesquisar,text="Todos",command=popular)
btn_todos.pack(side="left",padx=10)
btn_deletar=tkinter.Button(quadroPesquisar,text="Deletar",command=deletar)
btn_deletar.pack(side="left",padx=10)

#########Tabela Nomes##########
quadroGrid=tkinter.LabelFrame(app,text="Contatos")
quadroGrid.pack(fill="both",expand="yes",padx=10,pady=10)

# Tamanho da treeView
tv=ttk.Treeview(quadroGrid,columns=('id_contato','nome','telefone'), show='headings')
tv.column('id_contato',minwidth=0,width=50)
tv.column('nome',minwidth=0,width=450)
tv.column('telefone',minwidth=0,width=200)
tv.heading('id_contato',text='ID_CONTATO')
tv.heading('nome',text='NOME')
tv.heading('telefone',text='TELEFONE')
tv.pack()

popular()

app.mainloop()
