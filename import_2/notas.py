import os
import tkinter


pastaApp=os.path.dirname(__file__)  # acho q nao precisa, mas tudo bem!
print('Pasta da aplicação:', pastaApp)

def pesquisar():
    #usando "os.path.join" nao me preocupo se é windows ou linux
    arq_py = os.path.join(pastaApp, "pesquisar.py")
    
    # leva o contexto local junto com o modulo, o contexto global foi limpo {}
    # os imports podem ser feitos nos outros modulos normalmente
    # ou passados no topo deste arquivo, mas nunca aqui localmente
    exec(open(arq_py).read(), {})


app=tkinter.Tk()
app.title("Cadastro Fornecedor")
app.geometry("500x300")
app.configure(background="#dde")

# Menu 1
# Itens do Menu
barraDeMenus=tkinter.Menu(app)
menuContatos=tkinter.Menu(barraDeMenus,tearoff=0)
menuContatos.add_command(label="Pesquisar",command=pesquisar)
menuContatos.add_separator()
menuContatos.add_command(label="Fechar",command=quit)

# Definir Menu:
barraDeMenus.add_cascade(label="Contatos",menu=menuContatos)

app.config(menu=barraDeMenus)
app.mainloop()
