# Grava no Banco de Dados .db

from tkinter import *
import os
import banco


def gravarDados():
   # dentro de um METODO ou FUNCAO, as variaveis criadas fora nao sao vistas aqui
   # entao, deve-se passar como parametro, variavel global ou acessar a classe q
   # ela pertence.
   # Nesse caso abaixo, é uma variavel criada FORA do METODO (nao tem return no fim do codigo)
   # entao, ele nao conhece esta variavel.
   # Como este METODO esta sendo acionado no COMMAND de um BUTTON, nao se pode passar
   # esta variavel como parametro
   # ainda nao tem acesso!
   if tb_nome.get() != "":
       vnome=tb_nome.get()
       vfone=tb_fone.get()
       vemail=tb_email.get()
       vobs=tb_obs.get("1.0",END)
       vquery="INSERT INTO tb_contato (nome, telefone, email, obs) VALUES ('"+vnome+"','"+vfone+"','"+vemail+"','"+vobs+"')"
       banco.dml(vquery)
       tb_nome.delete(0,END)
       tb_fone.delete(0, END)
       tb_email.delete(0, END)
       tb_obs.delete("1.0", END)
       print("Dados Gravados")
   else:
       print("\nNome da pessoa é obrigatório!")


app = Tk()
app.title("Cadastro Fornecedor")
app.geometry("500x300")
app.configure(background="#dde")

# anchor=>N=Norte, S=Sul, E=Leste, W=Oeste
#        NE=Nordeste, SE=Sudeste, SO=Sudoeste, NO=Noroeste
Label(app, text="Nome", background="#dde", foreground="#009", anchor=W).place(x=10,y=10,width=100,height=20)

tb_nome = Entry(app)
tb_nome.place(x=10,y=30,width=300,height=20)

Label(app, text="Telefone", background="#dde", foreground="#009", anchor=W).place(x=10,y=60,width=100,height=20)
tb_fone=Entry(app)
tb_fone.place(x=10,y=80,width=100,height=20)

Label(app, text="E-mail", background="#dde", foreground="#009", anchor=W).place(x=10,y=110,width=100,height=20)
tb_email=Entry(app)
tb_email.place(x=10,y=130,width=300,height=20)

Label(app, text="OBS", background="#dde", foreground="#009", anchor=W).place(x=10,y=160,width=100,height=20)
tb_obs=Text(app)
tb_obs.place(x=10,y=180,width=300,height=80)

btn_salvar = Button(app, text="gravar", command=gravarDados)
btn_salvar.place(x=10,y=270,width=100,height=20)

app.mainloop()
