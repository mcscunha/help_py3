import os
import tkinter

#
# como vou chamar o modulo em uma instancia "bem limitada", deve-se importar
# tudo antes de chamar o "exec", depois nao pode importar mais bibliotecas!
#
from tkinter import ttk, messagebox
import banco


pastaApp=os.path.dirname(__file__)  # acho q nao precisa, mas tudo bem!
print('Pasta da aplicação:', pastaApp)

def pesquisar():
    #usando "os.path.join" nao me preocupo se é windows ou linux
    arq_py = os.path.join(pastaApp, "pesquisar.py")
    
    #o exec abaixo só funciona se o banco.py for importado no topo do codigo.
    #na maneira abaixo, o python nao deixa dar import de forma local ou nos 
    #arquivos que ele chamar agora, ou seja, esta instancia somente vai usar
    #os imports ja feitos no topo deste arquivo.
    #Isso serva para proteger o sistema contra acidentes!
    exec(open(arq_py).read())

def novocontato():
    arq_py = os.path.join(pastaApp, "novocontato.py")
    exec(open(arq_py).read(), {})


app=tkinter.Tk()
app.title("Cadastro Fornecedor")
app.geometry("500x300")
app.configure(background="#dde")

# Menu 1
# Itens do Menu
barraDeMenus=tkinter.Menu(app)
menuContatos=tkinter.Menu(barraDeMenus,tearoff=0)
menuContatos.add_command(label="Novo Contato",command=novocontato)
menuContatos.add_command(label="Pesquisar",command=pesquisar)
menuContatos.add_separator()
menuContatos.add_command(label="Fechar",command=quit)

# Definir Menu:
barraDeMenus.add_cascade(label="Contatos",menu=menuContatos)

app.config(menu=barraDeMenus)
app.mainloop()
