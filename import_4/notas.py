# Grava no Banco de Dados .db

import os
import tkinter


pastaApp=os.path.dirname(__file__)  # acho q nao precisa, mas tudo bem!
print('Pasta da aplicação:', pastaApp)

def pesquisar():
    #usando "os.path.join" nao me preocupo se é windows ou linux
    arq_py = os.path.join(pastaApp, "pesquisar.py")
    
    #quando se usa EXEC deve-se informar o escopo global ou local
    print('\nEscopo global:', globals())

    # leva todo o contexto global na execucao deste outro exec
    # somente passe globals() se vc quer alterar algo deste arquivo
    exec(open(arq_py).read(), globals())


app=tkinter.Tk()
app.title("Cadastro Fornecedor")
app.geometry("500x300")
app.configure(background="#dde")

# Menu 1
# Itens do Menu
barraDeMenus=tkinter.Menu(app)
menuContatos=tkinter.Menu(barraDeMenus,tearoff=0)
menuContatos.add_command(label="Pesquisar",command=pesquisar)
menuContatos.add_separator()
menuContatos.add_command(label="Fechar",command=quit)

# Definir Menu:
barraDeMenus.add_cascade(label="Contatos",menu=menuContatos)

app.config(menu=barraDeMenus)
app.mainloop()
